<?php

namespace App\Modules;

use GuzzleHttp\Client;

class ClientCurl
{
    public function init($method, $url, $base_api_url, $params = [])
    {
        $client = new Client(['base_uri' => $base_api_url]);

        $request_params = [];

        if (!empty($params) && $method == 'POST') {
            $request_params['headers'] = [ 'content-type' => 'application/json' ];
            $request_params['body'] = json_encode($params);
        }

        $response =  $client->request(
            $method,
            $base_api_url.$url,
            $request_params
        );

        return $response;
    }
}
