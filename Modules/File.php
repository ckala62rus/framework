<?php

namespace App\Modules;

/*
 * Файл для работы с загружаемыми файлами
 */
class File implements iFile
{
    public $file;
    public $uploaddir = __DIR__ . '/../Files/';

    public function __construct( $file )
    {
        if ( $file['error'] != 0 ) {
            return $this->getError( $file['error'] ) ;
        }

        $this->file = $file;
    }

    /*путь к файлу*/
    public function getPath()
    {
        return $this->uploaddir;
    }

    /*папка файла*/
    public function getDir()
    {
        return realpath($this->getPath());
    }

    /*имя файла*/
    public function getName()
    {
        return $this->file['name'];
    }

    /*расширение файла*/
    public function getExt()
    {
        $file_info = pathinfo($this->uploaddir . $this->getName());
        return  $file_info['extension'];
    }

    /*размер файла*/
    public function getSize()
    {
        return $this->file["size"];
    }

    /*получает текст файла*/
    public function getText()
    {
        $file_info = pathinfo($this->uploaddir . $this->getName());
        return $file_info["filename"];
    }

    /*устанавливает текст файла*/
    public function setText($text)
    {
        return rename($this->getPath(), $text);
    }

    /*добавляет текст в конец файла*/
    public function appendText($text)
    {
        return $this->getText() . $text;
    }

    /*копирует файл*/
    public function copy($copyPath)
    {
        // TODO: Implement copy() method.
    }

    /*удаляет файл*/
    public function delete()
    {
        return unlink($this->getPath() . $this->getName());
    }

    /*переименовывает файл*/
    public function rename($newName)
    {
        // TODO: Implement rename() method.
    }

    /*перемещает файл*/
    public function replace($newPath = null)
    {
        if ($newPath) {
            $this->uploaddir = $this->uploaddir . $newPath; //папка в нутри директории Files
        }

        if ( move_uploaded_file( $this->file['tmp_name'], $this->getPath() . basename( $this->getName() ) ) ) {
            return true;
        }

        return false;
    }

    /*
     * Получение ошибка при загрузке файла
     */
    public function getError( $error_code )
    {
        $errors = [
            1 => 'Размер принятого файла превысил максимально допустимый размер, который задан директивой upload_max_filesize конфигурационного файла php.ini.',
            2 => 'Размер загружаемого файла превысил значение MAX_FILE_SIZE, указанное в HTML-форме.',
            3 => 'Загружаемый файл был получен только частично.',
            4 => 'Файл не был загружен.',
            6 => 'Отсутствует временная папка. Добавлено в PHP 5.0.3.',
            7 => 'Не удалось записать файл на диск. Добавлено в PHP 5.1.0.',
            8 => 'PHP-расширение остановило загрузку файла. PHP не предоставляет способа определить, какое расширение остановило загрузку файла; в этом может помочь просмотр списка загруженных расширений с помощью phpinfo(). Добавлено в PHP 5.2.0.',
        ];

        return $errors[$error_code] ?? 'Неизвестная ошибка';
    }

}
