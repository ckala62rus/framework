<?php

namespace App\Modules;

/*
 * Класс для вывода шаблонов
 */
class View
{
    const PATH_VIEW = __DIR__ . '/../Views/';
    const PATH_LAYOUT = self::PATH_VIEW . 'layout/';

    /*
     * Вывод шаблона с хедером и футером, а так же телом
     */
    public function render($pattern = null)
    {
        //подключение хэдера сайта
        if ( file_exists( self::PATH_LAYOUT . 'header.php') ) {
            require self::PATH_LAYOUT . 'header.php';
        }

        if ( !empty( $pattern ) ) {
            if ( file_exists( self::PATH_VIEW . $pattern . '.php' ) ) {
                require self::PATH_VIEW . $pattern . '.php';
            }
        }

        //подключение футера сайта
        if ( file_exists( self::PATH_LAYOUT . 'footer.php') ) {
            require self::PATH_LAYOUT . 'footer.php';
        }

    }
}
