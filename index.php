<?php

use App\Modules\Database;

ini_set("display_errors", 1);
error_reporting(E_ALL);

require 'Modules/config.php';
require 'vendor/autoload.php';
require ('Controllers/Controller.php');

$bd = new Database(); //Создание подлючения для БД

$ctr = ucfirst(explode('/', $_SERVER['REQUEST_URI'])[1] ? explode('/', $_SERVER['REQUEST_URI'])[1] : 'site');
$act = explode('/', $_SERVER['REQUEST_URI'])[2] ? explode('/', $_SERVER['REQUEST_URI'])[2] : 'index';

$path = __DIR__ . '/Controllers/' . ucfirst($ctr) . 'Controller' . '.php';

if ( file_exists($path) ) {
    require $path;
}

$ctr_name = '\App\Controllers\\' . $ctr . 'Controller';
$controller = new $ctr_name;
$controller->$act();
