<?php

namespace App\Models;

class User
{
    public $name = '';
    public $patronumic = '';

    public function setName( $name )
    {
        $this->name = $name;
        return $this;
    }

    public function setLastName( $patronumic )
    {
        $this->patronumic = $patronumic;
        return $this;
    }

    public function getUserInfo()
    {
        return $this->name . ' ' . $this->patronumic;
    }
}
