<?php

namespace App\Controllers;

use App\Modules\ClientCurl;
use App\Modules\File;
use App\Modules\View;
use Katzgrau\KLogger\Logger;
use App\Modules\Database;

class SiteController extends Controller
{
    public $logger;

    public function __construct()
    {
        $this->logger = new Logger(__DIR__.'/../Errors');
    }

    public function index()
    {
        $params = [
            "name" => "morpheus",
            "job" => "leader",
        ];

        $response = (new ClientCurl())->init('POST', 'api/users', BASIC_URL_API, $params);

        dd( json_decode( $response->getBody()->getContents() ) );
        if ( $response->getStatusCode() == 200 ) {
            $result = json_decode( $response->getBody()->getContents() );
        }

        foreach ($result->data as $item) {
            dump($item->email);
        }

        dd('END');
//        $this->logger->info('Logger test!');
        $a = true;
//        $a = false;
        try {
            if ($a) {
                dump('Все хорошо!');
            } else {
                throw new \Exception( ' $a не равна 1');
            }
        } catch (\Exception $e) {
            $path = getcwd() . '/Errors/Errors.txt';
            $error_content = '[' . date('Y-m-d H:i:s') . ']:' . ' ' . 'Error in: ' . $e->getFile() . ' | ' . $e->getLine() . ' line |' . $e->getMessage() . "\n";
            file_put_contents($path, $error_content, FILE_APPEND);
        }
    }

    /*
     * Форма для загрузки файлов
     */
    public function formForFile()
    {
        $view = new View();
        echo $view->render( 'site/file-form' );
    }

    /*
     * Сохранение файлов
     */
    public function saveFile()
    {
        $file = new File( $_FILES['inputfile'] );
//        dd($file);
        $file->replace();
        header("Location: " . BASIC_URL . "/site/formForFile");
    }
}
